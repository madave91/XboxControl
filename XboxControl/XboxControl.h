#pragma once
#ifndef _XBOX_CONTROL_H_
#define _XBOX_CONTROL_H_

#include <windows.h>
#include <XInput.h>

class CXBOXController
{
private:
	XINPUT_STATE _controllerState;
	int _controllerNum;

public:
	CXBOXController(int playerNumber);
	XINPUT_STATE GetState();
	bool isConnected();
	void Vibrate(int leftVal = 0, int rightVal = 0);
};

#include "resource.h"
#endif