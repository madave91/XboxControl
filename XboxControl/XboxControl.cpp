// XboxControl.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "XboxControl.h"
#include <Windows.h>
#include <Xinput.h>
#include "stdafx.h"
//#include <afxwin.h>



#define MAX_LOADSTRING 100
#define WIN32_LEAN_AND_MEAN
#pragma comment(lib, "XInput.lib")

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//Local Functions:
void c_Commands(CXBOXController* Controller);
void java_Commands(CXBOXController* Controller);
void python_Commands(CXBOXController* Controller);
void cSharp_Commands(CXBOXController* Controller);
HWND getHWND();

//XBOX:
CXBOXController* Controller;

CXBOXController::CXBOXController(int playerNumber)
{
	_controllerNum = playerNumber - 1;
}

XINPUT_STATE CXBOXController::GetState()
{
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	XInputGetState(_controllerNum, &_controllerState);

	return _controllerState;
}

bool CXBOXController::isConnected()
{
	// Zeroise the state
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	// Get the state
	DWORD Result = XInputGetState(_controllerNum, &_controllerState);

	if (Result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CXBOXController::Vibrate(int leftVal, int rightVal)
{
	// Create a Vibraton State
	XINPUT_VIBRATION Vibration;

	// Zeroise the Vibration
	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;

	// Vibrate the controller
	XInputSetState(_controllerNum, &Vibration);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	/*
	XINPUT_GAMEPAD_DPAD_UP          0x00000001
	XINPUT_GAMEPAD_DPAD_DOWN        0x00000002
	XINPUT_GAMEPAD_DPAD_LEFT        0x00000004
	XINPUT_GAMEPAD_DPAD_RIGHT       0x00000008
	XINPUT_GAMEPAD_START            0x00000010
	XINPUT_GAMEPAD_BACK             0x00000020
	XINPUT_GAMEPAD_LEFT_THUMB       0x00000040
	XINPUT_GAMEPAD_RIGHT_THUMB      0x00000080
	XINPUT_GAMEPAD_LEFT_SHOULDER    0x0100
	XINPUT_GAMEPAD_RIGHT_SHOULDER   0x0200
	XINPUT_GAMEPAD_A                0x1000
	XINPUT_GAMEPAD_B                0x2000
	XINPUT_GAMEPAD_X                0x4000
	XINPUT_GAMEPAD_Y                0x8000
	bLeftTrigger, bRightTrigger, sThumbLX, sThumbLY, sThumbRX, and sThumbRY

	*/
	//XBOX MAIN METHODS:
	Controller = new CXBOXController(1);
	
	/*programStatus: 
		0 - C++
		1 - Java
		2 - Python
		3 - C#
	*/
	int programStatus = 0;
	HWND hwnd;

	while (true)
	{
		if (Controller->isConnected())
		{
			if (programStatus == 0)
			{
				c_Commands(Controller);
			}
			if (programStatus == 1)
			{
				java_Commands(Controller);
			}
			if (programStatus == 2)
			{
				python_Commands(Controller);
			}
			if (programStatus == 3)
			{
				cSharp_Commands(Controller);
			}
			
			//Non-specific operators (arrows, enter, space)
			if (Controller->GetState().Gamepad.bRightTrigger == 255)
			{
				keybd_event(VK_RETURN, 1, 0, 0);
				keybd_event(VK_RETURN, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}
			if (Controller->GetState().Gamepad.bLeftTrigger == 255)
			{
				keybd_event(VK_SPACE, 1, 0, 0);
				keybd_event(VK_SPACE, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}
			if (Controller->GetState().Gamepad.sThumbLX > -25000 & Controller->GetState().Gamepad.sThumbLX < -10000)
			{
				keybd_event(VK_LEFT, 1, 0, 0);
				keybd_event(VK_LEFT, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}
			if (Controller->GetState().Gamepad.sThumbLX < -25000)
			{
				keybd_event(VK_LEFT, 1, 0, 0);
				keybd_event(VK_LEFT, 1, KEYEVENTF_KEYUP, 0);
				Sleep(10);
			}
			if (Controller->GetState().Gamepad.sThumbLX < 25000 & Controller->GetState().Gamepad.sThumbLX > 10000)
			{
				keybd_event(VK_RIGHT, 1, 0, 0);
				keybd_event(VK_RIGHT, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}
			if (Controller->GetState().Gamepad.sThumbLX > 25000)
			{
				keybd_event(VK_RIGHT, 1, 0, 0);
				keybd_event(VK_RIGHT, 1, KEYEVENTF_KEYUP, 0);
				Sleep(10);
			}
			if (Controller->GetState().Gamepad.sThumbLY > -25000 & Controller->GetState().Gamepad.sThumbLY < -10000)
			{
				keybd_event(VK_DOWN, 1, 0, 0);
				keybd_event(VK_DOWN, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}
			
			if (Controller->GetState().Gamepad.sThumbLY < -25000)
			{
				keybd_event(VK_DOWN, 1, 0, 0);
				keybd_event(VK_DOWN, 1, KEYEVENTF_KEYUP, 0);
				Sleep(10);
			}
			if (Controller->GetState().Gamepad.sThumbLY < 25000 & Controller->GetState().Gamepad.sThumbLY > 10000)
			{
				keybd_event(VK_UP, 1, 0, 0);
				keybd_event(VK_UP, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}
			if (Controller->GetState().Gamepad.sThumbLY > 25000)
			{
				keybd_event(VK_UP, 1, 0, 0);
				keybd_event(VK_UP, 1, KEYEVENTF_KEYUP, 0);
				Sleep(10);
			}
			
			if (Controller->GetState().Gamepad.sThumbRX > 25000)
			{
				keybd_event(VK_TAB, 1, 0, 0);
				keybd_event(VK_TAB, 1, KEYEVENTF_KEYUP, 0);
				Sleep(100);
			}



			//Status changer:
			if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER)
			{
				if (programStatus == 3)
				{
					programStatus = 0;
					MessageBox(NULL, L"Program Status Changed", L"XboxControl", MB_OK);
				}
				else 
				{
					programStatus++;
					MessageBox(NULL, L"Program Status Changed", L"XboxControl", MB_OK);
				}
			}
			if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER)
			{
				if (programStatus == 0)
				{
					programStatus = 3;
					MessageBox(NULL, L"Program Status Changed", L"XboxControl", MB_OK);
				}
				else
				{
					programStatus--;
					MessageBox(NULL, L"Program Status Changed", L"XboxControl", MB_OK);
				}
			}
			
		}
		else
		{
			MessageBox(NULL, L"The XboxControl's terimnated", L"XboxControl", MB_OK);
			break;
		}
	}

	delete(Controller);

	return(0);
	

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_XBOXCONTROL, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_XBOXCONTROL));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//COMMANDS:

void c_Commands(CXBOXController* Controller)
{
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 0);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"std::cout <<\"\"<< std::endl;\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(0, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"while(true)\n{\n\t\n}\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_X)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"for(int i=0; i< ; i++)\n{\n\t\n}\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_Y)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"try{\n\t\n} catch(Exception e1){\n\t\n}\n");
		Sleep(500);
		Controller->Vibrate();
	}
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_START)
	{
		MessageBox(NULL, L"The XboxControl's status: C++", L"XboxControl", MB_OK);
	}
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
	{
		MessageBox(NULL, L"The XboxControl's terimnated", L"XboxControl", MB_OK);
		exit(0);
	}
}

void java_Commands(CXBOXController* Controller)
{
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 0);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"System.out.println(\"\");\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(0, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"while(true)\n{\n\t\n}\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_X)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"for(int i=0; i< ; i++)\n{\n\t\n}\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_Y)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"try{\n\t\n} catch(Exception e1){\n\t\n}\n");
		Sleep(500);
		Controller->Vibrate();
	}
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_START)
	{
		MessageBox(NULL, L"The XboxControl's status: Java", L"XboxControl", MB_OK);
	}
	
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
	{
		MessageBox(NULL, L"The XboxControl's terimnated", L"XboxControl", MB_OK);
		exit(0);
	}
}

void python_Commands(CXBOXController* Controller)
{
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 0);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"print(\"\")");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(0, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"sock = socket.socket(socket.AD_INET, socket.SOCK_STREAM)\n\t server_address = (address, port)\n sock.connect(server_address)\n input = [sock]");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_X)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"def setupServer(self, addr, port):\n\t server = socket.socket(socket.AD_INET, socket.SOCK_STREAM)\n server.setblocking(0)\n server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)\n server_address = (addr, port)\n server.bind(server_address)\n server.listen(5)\n return server\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_Y)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"def handleNewConnection(self, sock):\n\t connection, client_address = sock.accept()\n connection.settimeout(1.0)\n data = connection.recv(20)\n");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_START)
	{
		MessageBox(NULL, L"The XboxControl's status: Python", L"XboxControl", MB_OK);
	}
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
	{
		MessageBox(NULL, L"The XboxControl's terimnated", L"XboxControl", MB_OK);
		exit(0);
	}
}

void cSharp_Commands(CXBOXController* Controller)
{
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 0);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"some c# code...");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(0, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"some c# code...");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_X)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"some c# code...");
		Sleep(500);
		Controller->Vibrate();
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_Y)
	{
		HWND hwnd = getHWND();
		Controller->Vibrate(65535, 65535);
		SendMessage(hwnd, EM_REPLACESEL, TRUE, (LPARAM)"some c# code...");
		Sleep(500);
		Controller->Vibrate();
	}
	
	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_START)
	{
		MessageBox(NULL, L"The XboxControl's status: C#", L"XboxControl", MB_OK);
	}

	if (Controller->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_BACK)
	{
		MessageBox(NULL, L"The XboxControl's terimnated", L"XboxControl", MB_OK);
		exit(0);
	}
}

HWND getHWND()
{
	POINT p;
	HWND hwnd = GetForegroundWindow();
	GetCursorPos(&p);
	hwnd = WindowFromPoint(p);
	return hwnd;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_XBOXCONTROL));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_XBOXCONTROL);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   //ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
